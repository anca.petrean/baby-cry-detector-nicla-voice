## Link- ul repositorului: https://gitlab.upt.ro/anca.petrean/baby-cry-detector-nicla-voice.git

## Acest repository conține codul proiectului meu care mi-a fost exportat de către platforma Edge Impulse.

### Pașii de instalare au fost următorii:

1. Mai întai am instalat următorele dependențe
```bash
sudo apt install arduino-cli
./install_lib_linux.sh
```

2. Apoi a urmat procesul flash pentru firmware-ul MCU (Microcontroller Unit) dar și al NDP (Neural Decision Processor) impreuna cu modelul
```bash
sudo ./flash_linux.sh
```
* In caz de se dorește doar update-ul modelului se poate face prin scriptul
```bash
sudo ./flash_linux_model.sh
```
